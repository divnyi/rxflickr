//
//  NetworkApiTests.swift
//  NetworkApiTests
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

@testable import RxFlickr
import XCTest
import RxSwift

class NetworkApiTests: XCTestCase {
    let disposeBag = DisposeBag()

    func testPhotoSearch() {
           let result: Observable<PhotoSearch> = RawNetworkImpl().fetch(request: PhotosRequest(text: "", page: 1))
           
           let expect = expectation(description: "got results")
           result.subscribe(onNext: { result in
               expect.fulfill()
               print(result)
           }, onError: { error in
               XCTFail("network error \(error)")
               expect.fulfill()
           })
               .disposed(by: self.disposeBag)
           
           waitForExpectations(timeout: 3.0, handler: nil)
       }
    
    func testPhotoSearchWithText() {
        let result: Observable<PhotoSearch> = RawNetworkImpl().fetch(request: PhotosRequest(text: "kittens", page: 1))
        
        let expect = expectation(description: "got results")
        result.subscribe(onNext: { result in
            expect.fulfill()
            print(result)
        }, onError: { error in
            XCTFail("network error \(error)")
            expect.fulfill()
        })
            .disposed(by: self.disposeBag)
        
        waitForExpectations(timeout: 3.0, handler: nil)
    }
}
