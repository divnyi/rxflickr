# How to run?

Pods are not included to git. You need to run `pod install` yourself,
or run `./run` script (either from shell or finder).

# Targets & Schemes

There are 3 targets: application itself, Unit tests, Network API tests.

Unit tests are attached to application scheme, while network API tests have their separate scheme.

# Naming conventions

To make class names shorter, these conventions are put into place:

* Impl suffix -- implementaion
* VC suffix -- view controller
* VM suffix -- view model

