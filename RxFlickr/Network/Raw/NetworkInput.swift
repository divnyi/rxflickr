//
//  RawNetworkInput.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import Foundation

enum NetworMethod: String {
    case get = "GET"
    case post = "POST"
}

protocol NetworkInput {
    var method: NetworMethod { get }
    func url() throws -> URL
    func body() throws -> Data?
}
