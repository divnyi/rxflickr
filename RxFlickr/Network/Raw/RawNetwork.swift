//
//  RawNetworkInput.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import Foundation
import RxAlamofire
import RxSwift
import Alamofire

enum NetworkError: Error {
    case malformedUrl(error: Error)
    case serializationIssue(error: Error)
    case deserializationIssue(error: Error)
}

protocol RawNetwork {
    func fetch<Type>(request: NetworkInput) -> Observable<Type> where Type: Decodable
}

class RawNetworkImpl: RawNetwork {
    private func fetch(request: NetworkInput) -> Observable<(HTTPURLResponse, Data)> {
        do {
            let url = try request.url()
            var urlrequest = URLRequest(url: url)
            do {
                urlrequest.httpBody = try request.body()
            } catch {
                return Observable.error(NetworkError.serializationIssue(error: error))
            }
            urlrequest.httpMethod = request.method.rawValue
            urlrequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            urlrequest.setValue("application/json", forHTTPHeaderField: "Accept")

            return RxAlamofire.request(urlrequest).responseData()
        } catch {
            return Observable.error(NetworkError.malformedUrl(error: error))
        }
    }
    
    func fetch<Type>(request: NetworkInput) -> Observable<Type> where Type: Decodable {
        self.fetch(request: request).map { response -> Type in
            let decoder = JSONDecoder()
            let data = response.1
            do {
                let object = try decoder.decode(Type.self, from: data)
                return object
            } catch {
                throw NetworkError.deserializationIssue(error: error)
            }
        }
    }
}
