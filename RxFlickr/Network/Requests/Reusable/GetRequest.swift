//
//  GetRequest.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

class GetRequest: NetworkInput {
    var method: NetworMethod = .get
    
    var scheme: String = Constant.Network.scheme
    var host: String = Constant.Network.host

    var endpoint: Constant.Network.Endpoint!
    var queryParams: [String: String] = [:]

    func body() throws -> Data? {
        return nil
    }
    
    func url() throws -> URL {
        var components = URLComponents()
        components.scheme = self.scheme
        components.host = self.host
        components.path = self.endpoint.rawValue
        components.queryItems = self.queryParams.map { (arg) -> URLQueryItem in
            let (key, value) = arg
            return URLQueryItem(name: key, value: value)
        }
        return try components.asURL()
    }
}
