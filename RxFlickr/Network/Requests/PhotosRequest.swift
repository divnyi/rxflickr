//
//  PhotosRequest.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import Foundation

class PhotosRequest: GetRequest {
    let text: String
    let page: Int
    
    init(text: String, page: Int) {
        self.text = text
        self.page = page
        
        super.init()
        
        self.endpoint = .services
        self.queryParams = [
            "method" : text.count == 0 ? "flickr.photos.getRecent" : "flickr.photos.search",
            "api_key" : "11c40ef31e4961acf4f98c8ff4e945d7",
            "format" : "json",
            "nojsoncallback" : "1",
            "text" : text,
            "page" : String(page),
        ]
    }
    
    func nextPage() -> PhotosRequest {
        PhotosRequest(text: self.text, page: self.page+1)
    }
}
