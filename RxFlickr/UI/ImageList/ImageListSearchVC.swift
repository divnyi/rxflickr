//
//  ImageListSearchVC.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit
import RxSwift

final class ImageListSearchVC: UIViewController,
UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating {
    //MARK: interface
    var searchOptions: [String] = []
    let searchItemSelected = ReplaySubject<String>.create(bufferSize: 1)
    
    //MARK: creation
    static func createVC() -> ImageListSearchVC {
        let vc = self.fromStoryboard(storyboardName: "ImageList")
        return vc
    }
    
    //MARK: - outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    //MARK: - local vars
    private var searchText: String = ""
    
    //MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
    }
    
    //MARK: - UI
    private func setupUI() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = Constant.UI.ImageListSearch.searchItemHeight
    }
    
    private func updateUI() {
        self.tableViewHeight.constant =
            Constant.UI.ImageListSearch.searchItemHeight *
            CGFloat(min(self.searchOptions.count,
                        Constant.UI.ImageListSearch.searchBarMaxCount))
        
        self.tableView.reloadData()
    }
    
    //MARK: - UISearchResultsUpdating
    func updateSearchResults(for searchController: UISearchController) {
        self.searchText = searchController.searchBar.text ?? ""
        
        self.updateUI()
    }
    
    //MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return self.searchOptions.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Basic",
                                                 for: indexPath)
        
        cell.textLabel?.text = self.searchOptions[indexPath.row]
        
        return cell
    }
    
    //MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {
        let text = self.searchOptions[indexPath.row]
        self.searchItemSelected.on(.next(text))
    }
}
