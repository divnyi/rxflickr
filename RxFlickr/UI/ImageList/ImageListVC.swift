//
//  ViewController.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Kingfisher

final class ImageListVC: UIViewController {
    private var vm: ImageListVM!
    //MARK: creation
    static func createVC(vm: ImageListVM) -> ImageListVC {
        let vc = self.fromStoryboard(storyboardName: "ImageList")
        vc.vm = vm
        return vc
    }
    
    //MARK: - outlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: - local vars
    private var searchController: UISearchController!
    private var searchTableVC: ImageListSearchVC!
    
    private let disposeBag = DisposeBag()

    //MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        self.subscribe()
    }
    
    //MARK: - UI
    private func setupUI() {
        self.searchTableVC = ImageListSearchVC.createVC()
        self.searchController = UISearchController(searchResultsController: self.searchTableVC)
        
        self.searchController.searchResultsUpdater = self.searchTableVC
        self.searchController.searchBar.delegate = self
        self.searchController.obscuresBackgroundDuringPresentation = false
        self.searchController.searchBar.placeholder = "Image Name"
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationItem.searchController = self.searchController
        self.definesPresentationContext = true
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = nil
        
        self.title = "Flickr"
    }

    //MARK: - subscribe
    let nextPage: Observable<Void> = Observable.empty()
    func subscribe() {
        self.subscribeToVM()
        self.subscribeToSearchVC()
    }
    
    func subscribeToVM() {
        let searchText = self.searchController.searchBar.rx.text.map { $0 ?? "" }
        let searchClicked = self.searchController.searchBar.rx.searchButtonClicked
        let searchCancelled = self.searchController.searchBar.rx.cancelButtonClicked
        
        let selectedItem = self.collectionView.rx.itemSelected.map { $0.row }
                
        let scrolledToBottom = self.scrolledToBottom()
        
        let output = self.vm.transform(input: ImageListVM.Input(
            searchText: searchText,
            searchButtonClicked: searchClicked.asObservable(),
            searchCancelled: searchCancelled.asObservable(),
            selectedItem: selectedItem,
            loadNextPage: scrolledToBottom
        ))
        
        // setup cells
        output.cells
            .asDriver(onErrorJustReturn: [])
            .drive(self.collectionView.rx.items(
                cellIdentifier: "ImageListCell",
                cellType: ImageListCell.self
            )) { (_, vm: ImageListCellVM, cell: ImageListCell) in
                cell.setup(vm: vm)
        }
        .disposed(by: self.disposeBag)

        // scroll to top on cell update
        output.cells
            .asDriver(onErrorJustReturn: [])
            .map { _ in CGPoint.zero }
            .drive(self.collectionView.rx.contentOffset)
            .disposed(by: self.disposeBag)
            
        // update search options on history updates
        output.searchHistory
            .asDriver(onErrorJustReturn: [])
            .drive(onNext: { [weak self] (searchOptions: [String]) in
                self?.searchTableVC.searchOptions = searchOptions
            })
            .disposed(by: self.disposeBag)
    }
    
    func subscribeToSearchVC() {
        let searchHistoryItemSelected = self.searchTableVC.searchItemSelected
            .asObserver()
        
        searchHistoryItemSelected
            .subscribe(onNext: { text in
                self.searchController.searchBar.text = text
                self.searchController.searchBar.endEditing(true)
            })
            .disposed(by: self.disposeBag)
    }
    
    func scrolledToBottom() -> Observable<Void> {
        return self.collectionView.rx.contentOffset
            .flatMap { offset -> Observable<Void> in
                self.isNearTheBottomEdge(contentOffset: offset)
                    ? Observable.just(Void())
                    : Observable.empty()
        }
        .skip(1)
        .throttle(.seconds(2),
                  scheduler: MainScheduler.instance)
    }
    
    func isNearTheBottomEdge(contentOffset: CGPoint) -> Bool {
        return contentOffset.y + self.collectionView.frame.size.height
            > self.collectionView.contentSize.height
    }
}

extension ImageListVC: UISearchBarDelegate {
    //MARK: - hide when not typing
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchController.showsSearchResultsController = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchController.showsSearchResultsController = false
    }
}

extension ImageListVC: UICollectionViewDelegateFlowLayout {
    //MARK: - 2 per row
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width/2.0
        let height = width * Constant.UI.ImageList.imageHeightToWidth
            + Constant.UI.ImageList.cellNameplateHeight

        return CGSize(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
