//
//  ImageListCell.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

class ImageListCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    func setup(vm: ImageListCellVM) {
        self.nameLabel.text = vm.title
        self.imageView.kf.setImage(with: vm.imageURL)
    }
}
