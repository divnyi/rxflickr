//
//  ImageDetailVC.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/2/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

final class ImageDetailVC: UIViewController {
    var vm: ImageDetailVM!
    //MARK: creation
    static func createVC(vm: ImageDetailVM) -> ImageDetailVC {
        let vc = self.fromStoryboard()
        vc.vm = vm
        return vc
    }

    //MARK: - outlets
    @IBOutlet private weak var imageView: UIImageView!
    
    //MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
    }
    
    //MARK: - UI
    private func setupUI() {
        self.title = self.vm.title
        self.imageView.kf.setImage(with: vm.imageURL)
    }
}
