//
//  Constants+Endpoint.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

extension Constant {
    class Network {
        static let scheme = "https"
        static let host = "api.flickr.com"
        
        enum Endpoint: String {
            case services = "/services/rest/"
        }
    }
}
