//
//  Constant+UI.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

extension Constant {
    class UI {
        class ImageListSearch {
            static let searchBarMaxCount = 5
            static let searchItemHeight: CGFloat = 30.0
        }
        class ImageList {
            static let imageHeightToWidth: CGFloat = 1.0
            static let cellNameplateHeight: CGFloat = 50.0
        }
    }
}
