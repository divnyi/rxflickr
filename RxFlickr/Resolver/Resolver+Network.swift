//
//  Resolver+Network.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

extension Resolver {
    class Network: SubResolver {
        lazy var raw = { [unowned self] () -> RawNetwork in
            return RawNetworkImpl()
        }
    }
}
