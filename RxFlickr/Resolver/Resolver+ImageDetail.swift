//
//  Resolver+ImageDetail.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/2/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

extension Resolver {
    class ImageDetail: SubResolver {
        var photo: Photo!
        
        lazy var vc = { [unowned self] () -> UIViewController in
            return ImageDetailVC.createVC(vm: self.vm())
        }
        lazy var vm = { [unowned self] () -> ImageDetailVM in
            return ImageDetailVM.from(photo: self.photo)
        }
    }
}
