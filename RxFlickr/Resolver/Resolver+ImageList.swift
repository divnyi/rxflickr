//
//  Resolver+ImageList.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

extension Resolver {
    class ImageList: SubResolver {
        var imageDetailCoordinator: ImageDetailCoordinator!
        
        lazy var vc = { [unowned self] () -> UIViewController in
            return ImageListVC.createVC(vm: self.vm())
        }
        lazy var vm = { [unowned self] () -> ImageListVM in
            return ImageListVMImpl(repo: self.resolver.repo.photo(),
                                   imageDetailCoordinator: self.imageDetailCoordinator)
        }
    }
}
