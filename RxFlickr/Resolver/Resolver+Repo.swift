//
//  Resolver+Repo.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

extension Resolver {
    class Repo: SubResolver {
        lazy var photo = { [unowned self] () -> PhotoRepository in
            return PhotoRepositoryImpl(rawNetwork: self.resolver.network.raw())
        }
    }
}
