//
//  Resolver.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

class Resolver: NSObject {
    class SubResolver {
        weak var resolver: Resolver!
        init(resolver: Resolver) {
            self.resolver = resolver
        }
    }
    
    lazy var imageList = ImageList(resolver: self)
    lazy var imageDetail = ImageDetail(resolver: self)
    lazy var network = Network(resolver: self)
    lazy var repo = Repo(resolver: self)
}
