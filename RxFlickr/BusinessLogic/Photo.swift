//
//  Photo.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import Foundation

struct Photo: Codable {
    var id: String
    var owner: String
    var secret: String
    var server: String
    var farm: Int
    var title: String
    var isPublic: Int
    var isFriend: Int
    var isFamily: Int
    
    var imageUrl: URL? {
        var components = URLComponents()

        components.scheme = "https"
        components.host = "farm\(farm).static.flickr.com"
        components.path = "/\(server)/\(id)_\(secret).jpg"
        
        return components.url
    }
    
    enum CodingKeys: String, CodingKey {
        case id, owner, secret, server, farm, title
        case isPublic = "ispublic"
        case isFriend = "isfriend"
        case isFamily = "isfamily"
    }
}
