//
//  PhotoPage.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

struct PhotoPage: Codable {
    var page: Int
    var pages: Int
    var perpage: Int
    var photo: [Photo]
}
