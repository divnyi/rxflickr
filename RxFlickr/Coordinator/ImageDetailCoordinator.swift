//
//  ImageDetailCoordinator.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

protocol ImageDetailCoordinator {
    func start(photo: Photo)
}

class ImageDetailCoordinatorImpl: ImageDetailCoordinator {
    // MARK: inputs
    private weak var navigationViewController: UINavigationController?
    
    init(navigationViewController: UINavigationController?) {
        self.navigationViewController = navigationViewController
    }
    
    // MARK: - coordinator
    func start(photo: Photo) {
        let resolver = Resolver()

        resolver.imageDetail.photo = photo
        
        let vc = resolver.imageDetail.vc()
        
        self.navigationViewController?.pushViewController(vc, animated: true)
    }
}
