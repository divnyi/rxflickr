//
//  AppCoordinator.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

class AppCoordinator: Coordinator {
    // MARK: inputs
    private weak var window: UIWindow?
    
    init(window: UIWindow) {
        self.window = window
    }
    
    // MARK: - local variables
    private var navigationController: UINavigationController?
    private var imageListCoordinator: ImageListCoordinator?

    // MARK: - coordinator
    func start() {
        self.navigationController = UINavigationController()

        self.imageListCoordinator = ImageListCoordinator(
            navigationViewController: self.navigationController
        )

        self.window?.rootViewController = self.navigationController
        self.window?.makeKeyAndVisible()
        
        self.imageListCoordinator?.start()
    }
}
