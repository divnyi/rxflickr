//
//  ImageListCoordinator.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

class ImageListCoordinator: Coordinator {
    // MARK: inputs
    private weak var navigationViewController: UINavigationController?
    
    init(navigationViewController: UINavigationController?) {
        self.navigationViewController = navigationViewController
    }
    
    // MARK: - coordinator
    func start() {
        let resolver = Resolver()

        resolver.imageList.imageDetailCoordinator = ImageDetailCoordinatorImpl(
            navigationViewController: self.navigationViewController
        )
        
        let vc = resolver.imageList.vc()
        
        self.navigationViewController?.viewControllers = [vc]
    }
}
