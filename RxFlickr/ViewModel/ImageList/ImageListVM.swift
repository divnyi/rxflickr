//
//  ImageListVM.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import Foundation
import RxSwift

class ImageListVM: RxViewModel {
    struct Input {
        let searchText: Observable<String>
        let searchButtonClicked: Observable<Void>
        let searchCancelled: Observable<Void>
        
        let selectedItem: Observable<Int>
        
        let loadNextPage: Observable<Void>
    }
    struct Output {
        let search: Observable<String>
        let cells: Observable<[ImageListCellVM]>
        let searchHistory: Observable<[String]>
    }

    func transform(input: Input) -> Output {
        fatalError("you need to override this method")
    }
}

class ImageListVMImpl: ImageListVM {
    private let repo: PhotoRepository
    private let imageDetailCoordinator: ImageDetailCoordinator
    init(repo: PhotoRepository,
         imageDetailCoordinator: ImageDetailCoordinator) {
        self.repo = repo
        self.imageDetailCoordinator = imageDetailCoordinator
    }
    
    //MARK: - local vars
    private var history = ReplaySubject<[String]>.create(bufferSize: 1)
    private let disposeBag = DisposeBag()

    //MARK: - transform
    override func transform(input: Input) -> Output {
        let search = self.search(input)
        let photos = self.photos(input, search: search)

        self.subscribeImageDetail(input, photos: photos)

        let cells = self.cells(photos: photos)
        let searchHistory = self.searchHistory(input)
        return Output(
            search: search,
            cells: cells,
            searchHistory: searchHistory
        )
    }
    
    func search(_ input: Input) -> Observable<String> {
        let search = Observable.of(input.searchText,
                                   input.searchCancelled.map { "" })
            .merge()

        return search
    }
    
    func photos(_ input: Input, search: Observable<String>) -> Observable<[Photo]> {
        let photos = search
            .flatMapLatest { [weak self] text -> Observable<[Photo]> in
                guard let self = self else { return Observable.empty() }
                return self.repo.photosSearch(text: text,
                                              page: 1,
                                              loadNextPageTrigger: input.loadNextPage)
                    .catchErrorJustReturn([])
        }
        
        return photos
    }
    
    func cells(photos: Observable<[Photo]>) -> Observable<[ImageListCellVM]> {
        let cells = photos.map { photos in
            photos.map { photo in
                ImageListCellVM.from(photo: photo)
            }
        }

        return cells
    }
    
    func subscribeImageDetail(_ input: Input, photos: Observable<[Photo]>) {
        input.selectedItem.withLatestFrom(photos) { idx, photos in photos[idx] }
            .subscribe(onNext: { [weak self] photo in
                self?.imageDetailCoordinator.start(photo: photo)
        })
            .disposed(by: self.disposeBag)
    }
    
    func searchHistory(_ input: Input) -> Observable<[String]> {
        let searchText = input.searchButtonClicked
            .withLatestFrom(input.searchText)
        
        let history = self.history.asObserver()
            .startWith([])
        
        searchText.withLatestFrom(history) { text, history in
            return [text] + history
        }
        .subscribe(onNext: { [weak self] history in
            self?.history.on(.next(history))
        })
            .disposed(by: self.disposeBag)
        
        return history
    }
}
