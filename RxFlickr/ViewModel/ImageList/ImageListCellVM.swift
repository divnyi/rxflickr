//
//  ImageListCellVM.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

struct ImageListCellVM: Equatable {
    var title: String
    var imageURL: URL?
    
    static func from(photo: Photo) -> ImageListCellVM {
        return ImageListCellVM(
            title: photo.title,
            imageURL: photo.imageUrl
        )
    }
}
