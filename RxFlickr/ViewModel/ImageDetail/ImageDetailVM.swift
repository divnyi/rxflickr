//
//  ImageDetailVM.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/2/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import UIKit

struct ImageDetailVM: Equatable {
    var title: String
    var imageURL: URL?
    
    static func from(photo: Photo) -> ImageDetailVM {
        return ImageDetailVM(
            title: photo.title,
            imageURL: photo.imageUrl
        )
    }
}
