//
//  RxViewModel.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

protocol RxViewModel {
  associatedtype Input
  associatedtype Output
  
  func transform(input: Input) -> Output
}

