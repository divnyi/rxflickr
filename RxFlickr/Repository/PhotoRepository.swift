//
//  PhotoRepository.swift
//  RxFlickr
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

import Foundation
import RxSwift

protocol PhotoRepository {
    func photosSearch(
        text: String,
        page: Int,
        loadNextPageTrigger trigger: Observable<Void>
    ) -> Observable<[Photo]>
}

class PhotoRepositoryImpl: PhotoRepository {
    private let network: RawNetwork
    init(rawNetwork: RawNetwork) {
        self.network = rawNetwork
    }
    
    func photosSearch(
        text: String,
        page: Int,
        loadNextPageTrigger trigger: Observable<Void>
    ) -> Observable<[Photo]> {
        return self.paginatedRequest(
            fromList: [],
            request: PhotosRequest(text: text, page: page),
            loadNextPageTrigger: trigger
        )
    }
    
    private func paginatedRequest(
        fromList currentList: [Photo],
        request: PhotosRequest,
        loadNextPageTrigger trigger: Observable<Void>
    ) -> Observable<[Photo]> {
        let result: Observable<PhotoSearch> = self.network.fetch(request: request)
        let paginated = result.flatMap { (photos: PhotoSearch) -> Observable<[Photo]> in
            let newList = currentList + photos.photos.photo
            if photos.photos.page < photos.photos.pages {
                return Observable.of(
                    Observable.just(newList),
                    Observable.never().takeUntil(trigger),
                    self.paginatedRequest(fromList: newList,
                                          request: request.nextPage(),
                                          loadNextPageTrigger: trigger)
                ).concat()
            } else { return Observable.just(newList) }
        }
        return paginated
    }
}
