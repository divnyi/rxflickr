//
//  RxFlickrTests.swift
//  RxFlickrTests
//
//  Created by Oleksii Horishnii on 3/1/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

@testable import RxFlickr

import XCTest
import RxSwift
import RxTest

class ImageListVMTests: BaseCase {
    var repo: MockedPhotoRepository!
    var coordinator: MockedImageDetailsCoordinator!
    
    override func setUp() {
        super.setUp()
        
        self.repo = MockedPhotoRepository()
        self.resolver.repo.photo = { self.repo }

        self.coordinator = MockedImageDetailsCoordinator()
        self.resolver.imageList.imageDetailCoordinator = self.coordinator
    }

    let testInputs = [
        Photo(id: "123",
              owner: "123",
              secret: "123",
              server: "123",
              farm: 123,
              title: "123",
              isPublic: 1,
              isFriend: 0,
              isFamily: 0)
    ]
    lazy var testOutputs = self.testInputs
        .map { ImageListCellVM.from(photo: $0) }

    func testCells() {
        let vm: ImageListVM = self.resolver.imageList.vm()
        
        let scheduler = TestScheduler(initialClock: 0)
        
        // GIVEN
        let networkResponse = scheduler.createHotObservable([
            .next(210, self.testInputs),
            .completed(211)
        ])
        self.repo.photosSearchOutput = networkResponse.asObservable()
        
        let searchText: TestableObservable<String> = scheduler.createHotObservable([
            .next(201, "")
        ])
        let searchButtonClicked: TestableObservable<Void> = scheduler.createHotObservable([])
        let searchCancelled: TestableObservable<Void> = scheduler.createHotObservable([])
        let selectedItem: TestableObservable<Int> = scheduler.createHotObservable([])
        let loadNextPage: TestableObservable<Void> = scheduler.createHotObservable([])

        let input = ImageListVM.Input(
            searchText: searchText.asObservable(),
            searchButtonClicked: searchButtonClicked.asObservable(),
            searchCancelled: searchCancelled.asObservable(),
            selectedItem: selectedItem.asObservable(),
            loadNextPage: loadNextPage.asObservable()
        )
        let output = vm.transform(input: input)

        // WHEN
        let cells = scheduler.start {
            output.cells.asObservable()
        }
                
        // THEN
        let correctCells = Recorded.events(
            .next(210, self.testOutputs)
        )
        
        XCTAssertEqual(cells.events, correctCells)
    }
}
