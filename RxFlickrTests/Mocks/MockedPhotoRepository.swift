//
//  MockedPhotoRepository.swift
//  RxFlickrTests
//
//  Created by Oleksii Horishnii on 3/2/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

@testable import RxFlickr
import XCTest
import RxSwift

class MockedPhotoRepository: PhotoRepository {
    struct PhotosSearchInput {
        var text: String
        var page: Int
        var loadNextPageTrigger: Observable<Void>
    }
    var photosSearchInput: PhotosSearchInput?
    var photosSearchOutput: Observable<[Photo]>!
    func photosSearch(text: String,
                      page: Int,
                      loadNextPageTrigger trigger: Observable<Void>) -> Observable<[Photo]> {
        self.photosSearchInput = PhotosSearchInput(
            text: text,
            page: page,
            loadNextPageTrigger: trigger
        )
        return self.photosSearchOutput
    }
}
