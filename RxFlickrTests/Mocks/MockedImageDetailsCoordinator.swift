//
//  MockedImageDetailsCoordinator.swift
//  RxFlickrTests
//
//  Created by Oleksii Horishnii on 3/2/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

@testable import RxFlickr
import XCTest

class MockedImageDetailsCoordinator: ImageDetailCoordinator {
    var input: Photo?
    var counter: Int = 0
    func start(photo: Photo) {
        self.input = photo
        self.counter += 1
    }
}
