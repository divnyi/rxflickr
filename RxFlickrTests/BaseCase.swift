//
//  BaseCase.swift
//  RxFlickrTests
//
//  Created by Oleksii Horishnii on 3/2/20.
//  Copyright © 2020 Oleksii Horishnii. All rights reserved.
//

@testable import RxFlickr
import XCTest
import RxSwift

class BaseCase: XCTestCase {
    var resolver: Resolver!
    var disposeBag: DisposeBag!
    
    override func setUp() {
        super.setUp()
        
        self.resolver = Resolver()
        self.disposeBag = DisposeBag()
    }
    
    override func tearDown() {
        self.resolver = nil
        self.disposeBag = nil
        
        super.tearDown()
    }
}
